+++
headless = true
+++

Personal page.

Currently a Data Scientist at [Avvale](https://www.avvale.com/).

Formerly a math student at [Universit&agrave; di Pisa](http://www.unipi.it) and [Scuola Normale
Superiore](https://www.sns.it).
