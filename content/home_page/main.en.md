+++
headless = true
+++

# Curriculum
My CV can be downloaded
[HERE](https://gitlab.com/Teiolass/curriculum/-/raw/master/cv.pdf).

# Inspirations

A brief list of works that directly conditioned my vision of the world.

- [**The Witness**](http://the-witness.net/news/media/). A witty and poetic
  puzzle game developed by Jonathan Blow's Thekla studio.
![A screenshot from the trailer of The Witness](home_page/the-witness-trailer.png)

- [**Zen and the Art of Motorcycle Maintenance**](
  https://www.goodreads.com/book/show/629.Zen_and_the_Art_of_Motorcycle_Maintenance).
  An insightful book written by Robert Pirsig.

# My favorite repos

- [**High School Thesis**](https://gitlab.com/teiolass/tesina). This is the
  small thesis I wrote for the high school final in 2018 at [Liceo
  Newton](https://www.liceonewton.it/). It is about Genetic Algorithms, with
  some basic ideas and some working examples (the problem of finding the
  maximum of a function and the travelling salesman problem). I still love the
  graphical side of the
  [presentation](https://gitlab.com/Teiolass/tesina/-/raw/master/pdf/marchetti_pres.pdf).

- **Elsewhere: Chapter 0**. A WIP game written from scratch. The repo is still private, but I am
  super excited about this project!

- [**Ising Model Simulation via Propp-Wilson
  Algorithm**](https://gitlab.com/Teiolass/Propp-Wilson/).
  This was a project made for an exam at university. The objective is to simulate
  the Ising model (i.e. extracting a configuration of spins on a graph with the
  correct probability distribution) via a modification of the Monte Carlo
  method. The Propp-Wilson algorithm is able to give the exact distribution, in
  contrast with more classical methods, which can only give a sequence of
  distributions converging to the target one. 

- [**Bachelor Degree Thesis on Lambda
  Calculus**](https://gitlab.com/Teiolass/system-f). This is my work for the
  final thesis at Università of Pisa, advisors Alessandro Berarducci and
  Marcello Mamino. It studies some typed variants of the lambda calculus,
  G&ouml;del's system and system F (Girard and Reynolds). The main result is a
  duality between the lambda terms (which can be seen as a subset computer
  programs) and proofs in the intuitionistic logic.

- [**Master Degree Thesis on Explainable AI**](https://github.com/Teiolass/tesi-magistrale-code).
  This is the code written for the experiments of my master thesis. In this project I built a
  transformer based model to predict the evolution of the clinical condition of patients from the
  Beath Israeli medical center. Then I built an explaination pipeline for a post-hoc local
  explaination. The focal point was to compare the attention matrices with the feature importances
  given by the explaination and show that _Attention is Not an Explaination_.

- [**Text classification via Compression
  Algorithms**](https://gitlab.com/Teiolass/lz78-classifier). Fourth year
  presentation at Scuola Normale Superiore, advisor Stefano Marmi. This is the
  presentation of a text classifier that measures the Kullback-Leibler distance
  between a reference corpus and a target text. The measurement is done by
  compressing the target on the corpuses via the Lempel-Ziv LZ78 algorithm.

- [**Dongim**](https://gitlab.com/Teiolass/dongim). Tiny and cute pomodoro timer. Timers
  are my favorite toy to learn a new programming language, and this is my Rust implementation!
  It's my favorite timer at the moment. 


- [**G2 Notes**](https://github.com/Marcov999/Appunti). Notes from Roberto
  Frigerio's course Geometria 2. I coauthored these notes together with [Marco
  Vergamini](https://uz.sns.it/~marco_vergamini) and Luigi Traino.

- [**Exact Aggregate Solutions for M/G/1 Queues**](https://gitlab.com/Teiolass/aggregate-solution-mg1-queue/).
  Seminar for the course Numerical Methods for Markov Chain. I presented a method to compute some
  entries of the stationary vector of the Markov Process associated to a M/G/1 type queue. These
  entries can be then used to compute the moments of the queue length.

- Many more!
