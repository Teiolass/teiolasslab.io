+++
headless = true
+++

Pagina personale.

Data Scientist presso [Avvale](https://www.avvale.com/).

Ex studente di matematica presso l'[Universit&agrave; di Pisa](http://www.unipi.it) e la
[Scuola Normale Superiore](https://www.sns.it).
