+++
headless = true
+++


# Curriculum
Scarica [QUI](https://gitlab.com/Teiolass/curriculum/-/raw/master/cv.pdf) il mio CV (ENG).

# Ispirazioni

Un breve elenco di opere che in qualche modo hanno influenzato in direttamente
il mio punto di vista sulle cose.

- [**The Witness**](http://the-witness.net/news/media/). Un brillante e poetico puzzle game
  sviluppato da Thekla, studio capitanato da Jonathan Blow.
![Uno screenshot dal trailer di The Witness](home_page/the-witness-trailer.png)

- [**Lo Zen e l'arte della manutenzione della
  motocicletta**](https://www.goodreads.com/book/show/629.Zen_and_the_Art_of_Motorcycle_Maintenance).
  Un bel libro di Robert Pirsig.

# Le mie repo preferite
- [**Tesina di maturit&agrave;**](https://gitlab.com/teiolass/tesina). Questa &egrave; stata la
  tesina che ho portato nel 2018 alla maturit&agrave; (ho fatto Scienze Applicate al [Liceo
  Newton](https://www.liceonewton.it/)). L'argomento che avevo scelto erano gli algoritmi genetici,
  che avevo scoperto grazie al buon [Daniel Shiffman](https://natureofcode.com/). Gi&agrave; dal
  titolo si prestava bene come argomento per via della sua "multidisciplinariet&agrave;", ma era
  stato molto interessante fare il tutto, portando tra gli esempi una soluzione approssimata del
  problema del commesso viaggiatore. La presentazione finale si pu&ograve; trovare
  [QUI](https://gitlab.com/Teiolass/tesina/-/raw/master/pdf/marchetti_pres.pdf?inline=false), sono
  ad oggi molto contento di come era uscita esteticamente.

- **Elsewhere: Chapter 0**. Un gioco ancora WIP scritto da zero senza game engines. La repo è ancora
  privata, ma sto sviluppando attivamente e sono super positivo riguardo questo progetto!

- [**Simulazione del Modello di Ising con l'algoritmo di
  Propp-Wilson**](https://gitlab.com/Teiolass/Propp-Wilson/).
  Progetto svolto per un corso universitario. L'obiettivo &egrave; quello di simulare il modello di
  Ising con una versione del metodo di Monte Carlo che fornisce (a differenza dell'algoritmo
  classico) una simulazione con la distribuzione esatta.
  [QUI](https://gitlab.com/Teiolass/Propp-Wilson/-/raw/master/relazione/la_relazione.pdf?inline=false)
  la relazione finale.

- [**Tesi di Laurea Triennale sul lambda calcolo**](https://gitlab.com/Teiolass/system-f). Qui si
  trova il mio lavoro per la tesi triennale (corso di laurea in Matematica, Universit&agrave; di
  Pisa). La trattazione principale &egrave; su alcune varianti tipate del lambda calcolo, il sistema
  T di G&ouml;del e il sistema F (Girard e Reynolds). La parte succosa &egrave; quella in cui si
  mostra che all'interno di questi calcoli &egrave; possibile rappresentare esattamente le funzioni
  che si dimostrano essere totali rispettivamente nella logica intuizionista del primo e del secondo
  ordine. In particolare &egrave; equivalente (a meno di un semplice cambio di sintassi) avere una
  dimostrazione di totalit&agrave; e un programma per il calcolo della funzione. Link veloci per la
  [tesi](https://gitlab.com/Teiolass/system-f/-/raw/master/tesi_marchetti.pdf?inline=false) e la
  [presentazione](https://gitlab.com/Teiolass/system-f/-/raw/master/ts/la_presentazione.pdf?inline=false).

- [**Tesi di Laurea Triennale in Explainable AI**](https://github.com/Teiolass/tesi-magistrale-code). 
  Questo è il codice per gli esperimenti relativi alla mia laurea magistrale. In questa tesi ho
  costruito un'architettura transformer per fare predizioni su dati medici (dal dataset Mimic-iv) e
  poi aumentarli con una spiegazione. Il punto focale è stato quello di osservare che le matrici di
  attenzione che si trovano nei transformer correlano male con le feature importances date dalle
  spiegazioni, e che quindi in un certo senso _Attention is not an Explanation_.

- [**Classificazione di testi con algoritmi di
  compressione**](https://gitlab.com/Teiolass/lz78-classifier). Colloquio del quarto anno alla SNS. Il
  tema principale &egrave; quello di classficare un dataset di messaggi tra spam e non spam
  utilizzando tecniche di teoria dell'informazione. Si analizza in particolare l'algoritmo di
  Lempel-Ziv LZ78 (che &egrave; anche presente per esempio nella compressione lossless di immagini
  png).
  [QUI](https://gitlab.com/Teiolass/lz78-classifier/-/raw/master/pres/la_presentazione.pdf?inline=false)
  la presentazione finale.

- [**Dongim**](https://gitlab.com/Teiolass/dongim). Una piccolo e comodo timer pomodoro. Per imparare
  un linguaggio di programmazione i timer sono il mio giocattolo preferito. Questa è l'implementazione
  in Rust! Ad oggi, è il mio timer preferito.

- [**Appunti di G2**](https://github.com/Marcov999/Appunti). Appunti del corso del secondo anno di
  Geometria 2 tenuto dal professor Frigerio, scritti con Luigi Traino e [Marco
  Vergamini](https://uz.sns.it/~marco_vergamini).
  Link al file
  [PDF](https://raw.githubusercontent.com/Marcov999/Appunti/master/Geometria2/Geometria2.pdf).

- [**Soluzioni Aggregate Esatte per Code M/G/1**](https://gitlab.com/Teiolass/aggregate-solution-mg1-queue/).
  Un seminario per il corso di Metodi Numerici per Catene di Markov. Viene presentata un'alternativa
  alla formula di Ramaswami per calcolare alcune componenti del vettore stazionario di una coda di
  tipo M/G/1 e i momenti della lunghezza della stessa. [QUI](https://gitlab.com/Teiolass/aggregate-solution-mg1-queue/-/raw/master/exact-aggregate-mg1/la_presentazione.pdf)
  la presentazione finale.

- Tante altre!
